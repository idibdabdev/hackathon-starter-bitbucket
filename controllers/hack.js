const nodemailer = require("nodemailer");

/**
 * GET /hack
 * ThinkAnalytics Hack page
 */
exports.getContact = (req, res) => {
  const unknownUser = !req.user;

  res.render("hack", {
    title: "Hack Page - This is where the goodies are at!",
    unknownUser
  });
};

/**
 * POST /hack
 * This will be how we're sending content to the Hack Page Chart to render information
 */
exports.getHackDayChart = async (req, res, next) => {
  // Get your API key: https://www.alphavantage.co/documentation/
  const url = `https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=MSFT&outputsize=compact&apikey=${
    process.env.ALPHA_VANTAGE_KEY
  }`;
  axios
    .get(url)
    .then(response => {
      // handle success
      const arr = response.data["Time Series (Daily)"];
      let dates = []; // 7 days
      let closing = []; // stock closing value
      const keys = Object.getOwnPropertyNames(arr);
      for (let i = 0; i < 7; i++) {
        dates.push(keys[i]);
        closing.push(arr[keys[i]]["4. close"]);
      }
      // reverse so dates appear from left to right
      dates.reverse();
      closing.reverse();
      dates = JSON.stringify(dates);
      closing = JSON.stringify(closing);
      res.render("api/chart", {
        title: "Chart",
        dates,
        closing
      });
    })
    .catch(err => {
      // handle error
      next(err);
    });
};
