/**
 * Reads the input file into the system
 */
const Excel = require('exceljs');
const Movie = require('../models/Movie');
const Track = require('../models/Track');
const find = require('lodash/find');
const path = require('path');

const filePath = path.join(__dirname, '..','public','resources','hack_day_data.xlsx');


exports.performIngest = (req, res) => {

    let ingestPromise = new Promise(function(resolve,reject) {

        let excelPromise = readExcelFile();

        excelPromise.then(function(result){
            let spotifyPromise = performSpotifySearch(result);
            spotifyPromise.then(function(result){
                resolve(result);
            })
        });
    });

    ingestPromise.then(function(result){
        res.send(result);
    });

};

let performSpotifySearch = data => {

    return new Promise(function(resolve,reject){

        //TODO Do the spotify stuff per track


        resolve(data);
    });


};

let readExcelFile = () => {
    return new Promise(function (resolve, reject) {
        let data = [];

        let workbook = new Excel.Workbook();

        workbook.xlsx.readFile(filePath).then(function () {
            data = readMovies(workbook);
            data = readAlbum(data, workbook);
            data = readTrackInfo(data, workbook);

            resolve(data);
        });
    });

};

/**
 * Read the movie data from the xlsx sheet
 * @returns {Array}
 */
let readMovies = workbook => {
    let data = [];
    let worksheet = workbook.getWorksheet(("IMDB_TITLES"));

    //Excel indexes at 1
    worksheet.eachRow(function(row,rowNumber){
       if(rowNumber > 1){
           let url = row.getCell(1).text;
           let title = row.getCell(2).text;

           title = title.replace(/(\r\n|\n|\r)/gm,"");

           let genreString = row.getCell(3).text;

           let id = url.replace("https://www.imdb.com/title/tt","");
           let genres = genreString.split(",");

           let movie = new Movie(id,url,title,genres);
           data.push(movie);
       }
    });

    return data;
};

/**
 * Reads the album data from the xlsx sheet
 * @param data
 * @param workbook
 * @returns {*}
 */
let readAlbum = (data, workbook) => {

    let worksheet = workbook.getWorksheet(("IMDB_SOUNDTRACK_ALBUM"));


    //Excel indexes at 1
    worksheet.eachRow(function(row,rowNumber){
        if(rowNumber > 1){
            let url = row.getCell(1).text;
            let id = url.replace("https://www.imdb.com/title/tt","");

            let albumTitle = row.getCell(2).text;

            //TODO We dont have this?
            let albumSpotifyId = row.getCell(3).text;

            //Use _.matchesProperty iteratee shorthand to find the right movie
            let movie = find(data, ['imdbID', id]);
            if(typeof movie !== "undefined"){
                movie.albumTitle = albumTitle;
                movie.albumSpotifyId = albumSpotifyId;
            }else{
                console.log("Ingest found a track for an unknown movie: " + url);
            }
        }
    });

    return data;
};

/**
 *
 * @param data
 * @param workbook
 * @returns {*}
 */
let readTrackInfo = (data, workbook) => {

    let tracks = [];
    let worksheet = workbook.getWorksheet(("IMDB_SOUNDTRACK_TRACKLIST"));

    //Excel indexes at 1
    worksheet.eachRow(function(row,rowNumber){
        if(rowNumber > 1){
            let url = row.getCell(1).text;
            let id = url.replace("https://www.imdb.com/title/tt","");

            let trackTitle = row.getCell(4).text;
            let trackArtistString = row.getCell(5).text;
            let artists = trackArtistString.split(",");

            let track = new Track(id,"","",trackTitle,artists);
            tracks.push(track);
        }
    });

    for(let track of tracks){
        //Use _.matchesProperty iteratee shorthand to find the right movie
        let movie = find(data, ['imdbID', track.imdbID]);
        if(typeof movie !== "undefined"){
            let t = find(movie.tracks,{'title': track.title ,'artists': track.artists});
            if(typeof t === "undefined"){
                movie.tracks.push(track);
            }
        }else{
            console.log("Ingest found a track for an unknown movie: " + url);
        }
    }
    return data;
};
