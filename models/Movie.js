class Movie {

    /**
     * Constructs a Movie object
     * @param imdbID The IMDB ID (minus URL & prefix "tt"
     * @param url The full URL of this movie
     * @param title The movie Title
     * @param genres The list of genres for the movie
     * @param albumTitle The album title for the movie
     * @param albumSpotifyId The spotify ID of the album
     * @param tracks List of tracks for the item
     */
    constructor(imdbID = "", url = "", title = "", genres = [], albumTitle = "", albumSpotifyId = "", tracks = []) {
        this.imdbID = imdbID;
        this.url = url;
        this.title = title;
        this.genres = genres;
        this.albumTitle = albumTitle;
        this.albumSpotifyId = albumSpotifyId;
        this.tracks = tracks;
    }
}

// Exports the component so it is visible to other components as an import
module.exports = Movie;