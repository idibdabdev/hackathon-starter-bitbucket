class Track {

    /**
     * Represents a single track from a movie
     * @param imdbID The IMDB id of the movie containing this track
     * @param url The url of the movie
     * @param spotifyId The spotify ID of this track
     * @param title The title of this track
     * @param artists The artists for this track
     * @param audio_features_spotify The list of spotify features
     * @param audio_features_think The list of Think features
     */
    constructor(imdbID = "", url = "",spotifyId = "", title = "", artists = [], audio_features_spotify = {}, audio_features_think = {}) {
        this.imdbID = imdbID;
        this.url = url;
        this.spotifyId = spotifyId;
        this.title = title;
        this.artists = artists;
        this.audio_features_spotify = audio_features_spotify;
        this.audio_features_think = audio_features_think;
    }
}

// Exports the component so it is visible to other components as an import
module.exports = Track;